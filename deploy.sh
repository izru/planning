#!/bin/bash

echo "stopping planning_front"
for docker in $(docker ps | grep front | awk '{print$1}');
do
  docker stop $docker 2>/dev/null && docker rm -v $docker 2>/dev/null
done

echo "stopping planning_strapi"
for docker in $(docker ps |grep strapi|awk '{print$1}');
do
  docker stop $docker 2>/dev/null && docker rm -v $docker 2>/dev/null
done

echo "delete images"
docker rmi $(docker images -q) 2>/dev/null || echo "\033[32mcontainers deleted\033[32m"

echo "delete .strapi-updater.json"
rm -f strapi/.strapi-updater.json

echo "docker-compose start"
docker-compose up -d
