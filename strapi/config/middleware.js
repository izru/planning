module.exports = {
  load: {
    before: ['responseTime', 'logger', 'cors', 'responses', 'gzip'],
    after: ['parser', 'router', 'historyrecords']
  },
  settings: {
    historyrecords: {
      enabled: true
    }
  }
};
