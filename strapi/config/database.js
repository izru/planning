module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('MYSQL_DATABASE_HOST', 'mysql'),
        port: env.int('MYSQL_DATABASE_PORT', 3306),
        database: env('MYSQL_DATABASE_NAME', 'plandb'),
        username: env('DATABASE_USERNAME', 'strapi'),
        password: env('DATABASE_PASSWORD', 'strapi'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
    history: {
      connector: 'mongoose',
      settings: {
        host: env('MONGO_DATABASE_HOST', 'mongo'),
        srv: env.bool('MONGO_DATABASE_SRV', false),
        port: env.int('MONGO_DATABASE_PORT', 27017),
        database: env('MONGO_DATABASE_NAME', 'history'),
        username: env('DATABASE_USERNAME', 'strapi'),
        password: env('DATABASE_PASSWORD', 'strapi'),
      },
      options: {
        authenticationDatabase: env('AUTHENTICATION_DATABASE', null),
        ssl: env.bool('DATABASE_SSL', false),
      },
    },
  },
});
