require("dotenv").config();
const nodemailer = require("nodemailer");

module.exports = async (opt, mail) => {
  if (!mail.subject) {
    throw Error({ message: "Укажите subject" });
  }
  const transporter = nodemailer.createTransport({
    host: opt?.host || process.env.MAIL_HOST,
    port: opt?.port || process.env.MAIL_PORT,
    auth: {
      user: opt?.user || process.env.MAIL_USER,
      pass: opt?.pass || process.env.MAIL_PASS,
    },
    secure: opt?.secure || false,
    logger: true,
    debug: true,
  });
  return await transporter.sendMail(
    {
 	from: mail?.from,
	to: mail?.to,
      subject: mail?.subject,
      text: mail?.text || null,
      html: mail?.html || null,
    },
    (err, info) => {
      if (err) {
        console.log(err);
      }
      console.log(info);
    }
  );
};
