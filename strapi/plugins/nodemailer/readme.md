# Рассылка на чистом nodemailer

#### [Функция](./mailer.js)

### Пример:

```
const mailer = require('./plugins/nodemailer/mailer');

const start = async () => {
    try{
        let info = await mailer(opt, mail)
    } catch(e){
        console.log(e)
    } finally{
        console.log(---- info.messageId ----)
    }
}
```

### Параметры функции:
> строки с "!" помечены как обязательные  параметры
- opt:
> Первые 4 параметра имеют дефолтные значения от noreply, указанный в .env
```
{
    host: !str,         // ---- Хост 
    port: !int,         // ---- Порт
    user: !str          // ---- Логин/почта, с которой будет идти рассылка
    pass: !str          // ---- Пароль от пользователя
    secure: bool        // ---- Настройка подключения защиты (дефолт: false)
}
```
- mail:
> В данный объект нужно передавать само письмо, автора письма и кому оно придёт. 
```
{
    from: str           // ---- Отправитель (дефолт: obj.user)
    !to: str            // ---- Кому
    !subject: str       // ---- Заголовок письма
    text: str           // ---- Текст письма
    html: str           // ---- HTML-код для письма
}
```
