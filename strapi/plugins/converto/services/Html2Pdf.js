'use strict';

const puppeteer = require('puppeteer');

/**
 * @param {!function} pageInit
 * @param {!{format?: string, scale?: number, landscape?: Boolean, title?: string, date?: string}} options
 * @returns {Promise<*>}
 */
async function generatePdf(pageInit, options) {
  const browser = await puppeteer.launch({ args: ["--no-sandbox"] })
  const page = await browser.newPage();
  await pageInit(page);
  await page.addStyleTag({ path: 'plugins/converto/css/pdf.css' });
  const buffer = await page.pdf({
    format: options.format,
    scale: options.scale,
    landscape: options.landscape,
    printBackground: true,
    displayHeaderFooter: true,
    headerTemplate: `<div style="width:100%;position:relative;font-size:8px;text-align:center"><span style="position:absolute;left:20px;">${options.date}</span>${options.title}</div>`,
    footerTemplate: `<div style="width:100%;font-size:8px;padding:0 20px;text-align:right"><span class="pageNumber"></span>/<span class="totalPages"></span></div>`,
    margin: {
      top: 40,
      bottom: 40,
      left: 20,
      right: 20
    }
  });
  await browser.close();
  return buffer;
}

module.exports = {

  /**
   * @param {string} url URL to render.
   * @param {!{format?: string, scale?: number, landscape?: Boolean, title?: string, date?: string}=} options
   * @returns {Promise<Buffer>}
   */
  url2pdf: async (url, options = {}) => {
    return await generatePdf(async (page) => {
      await page.goto(url, {waitUntil: 'networkidle2'});
    }, options);
  },

  /**
   * @param {string} html
   * @param {!{format?: string, scale?: number, landscape?: Boolean, title?: string, date?: string}=} options
   * @returns {Promise<Buffer>}
   */
  html2pdf: async (html, options = {}) => {
    return await generatePdf(async (page) => {
      await page.setContent(html, {waitUntil: 'networkidle2'});
    }, options);
  }
};
