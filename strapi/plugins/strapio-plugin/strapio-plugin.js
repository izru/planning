/*  Helper Functions  */

const sendDataBuilder = (identity, entity) => {
  return Array.isArray(entity)
    ? JSON.stringify({ identity: identity.toLowerCase(), entity })
    : JSON.stringify({ identity: identity.toLowerCase(), ...entity });
};

const getUpServices = (strapi) => strapi.plugins["users-permissions"].services;

const sendMessageToSocket = (socket, message) => {
  socket.emit("message", message);
};

/* socket.io middleware */

const subscribe = (socket, next) => {
  socket.on("subscribe", (payload) => {
    if (payload !== undefined && payload !== "") {
      socket.join(payload.toLowerCase());
      sendMessageToSocket(
        socket,
        "Successfully joined: " + payload.toLowerCase()
      );
    }
  });
  next();
};

const handshake = (socket, next) => {
  if (socket.handshake.query && socket.handshake.query.token) {
    const upsServices = getUpServices(strapi);
    upsServices.jwt.verify(socket.handshake.query.token).then((user) => {
      sendMessageToSocket(socket, "handshake ok");
      upsServices.user
        .fetchAuthenticatedUser(user.id)
        .then((detail) => socket.join(detail.role.name));
    }).catch((err) => {
      sendMessageToSocket(socket, err.message);
      socket.disconnect()
    });
  } else {
    sendMessageToSocket(socket, "No token given.");
    socket.disconnect();
  }
  next();
};

/* socket.io actions */

const emit = (upsServices, io) => {
  return async (vm, action, entity) => {

    // send to specific subscriber
    if (entity._id || entity.id) {
      io.sockets
        .in(`${vm.identity.toLowerCase()}_${entity._id || entity.id}`)
        .emit(action, sendDataBuilder(vm.identity, entity));
    }

    // send to all in collection room
    io.sockets
      .in(vm.identity.toLowerCase())
      .emit(action, sendDataBuilder(vm.identity, entity));
  };
};

const StrapIO = (strapi, options) => {
  const io = require("socket.io")(strapi.server, options);

  // loading middleware ordered
  io.use(handshake);
  io.use(subscribe);

  // добавление записи в таблицу "Authorizations"
  io.on("connection", (socket) => {
    socket.on("login", () => {
      const user = JSON.parse(socket.handshake.query.user);
      strapi.services.authorization.create({
        user: user.email,
        session_id: socket.handshake.query.token,
        session_start: new Date(),
        session_end: null,
        info: {
          id: user.id,
          name: `${user.surname} ${user.name}`,
          email: user.email,
          role: user.role.name,
          address: socket.handshake.address,
          headers: socket.handshake.headers
        },
      });
    });
    socket.on("disconnecting", async () => {
      try {
        await strapi.query("authorization").find({ session_id: socket.handshake.query.token })
        .then(async (res) => {
          if (res && res.length) {
            const ms = new Date() - res[0].session_start;
            const session_duration = new Date(ms);
            await strapi.query("authorization").update({ id: res[0]._id },
              {
                session_end: new Date(),
                session_duration: `${session_duration.getHours()} ч ${session_duration.getMinutes()} мин`
              }
            );
          };
        });
      } catch(e) {
        throw(e);
      }
    });
  });

  // debugging
  if(process.env.DEBUG == "strapio" || process.env.DEBUG == "*") {
    io.on("connection", (socket) => {
      console.debug("Connected Socket:", socket.id);
      socket.on("disconnecting", (reason) => {
        console.debug("Socket Disconnect:", socket.id, socket.rooms);
      });
    });
  }


  return {
    emit: emit(getUpServices(strapi), io),
    emitRaw: (room, event, data) => io.sockets.in(room).emit(event, data),
  };
};

module.exports = StrapIO;
