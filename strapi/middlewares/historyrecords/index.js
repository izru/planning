module.exports = strapi => {
  return {
    initialize() {
      strapi.app.use(async (ctx, next) => {
        await next();
        if (ctx.request.method === 'DELETE' && ctx.req.user && ctx.request.url.includes('/shootings') &&
            !ctx.response.body.personal && !ctx.response.body.duty_group && !ctx.response.body.coordination_event)
        {
          let obj = {
            id: ctx.response.body.id,
            name: ctx.response.body.name,
            date_start: ctx.response.body.date_start,
            date_end: ctx.response.body.date_end
          }
          if (ctx.response.body.metatheme) {
            obj.metatheme = {
              id: ctx.response.body.metatheme.id,
              name: ctx.response.body.metatheme.name
            }
          }
          strapi.services.historyrecord.create({
            contentId: ctx.response.body.id,
            contentType: 'Съемка',
            name: ctx.response.body.name || '',
            action: 'delete',
            author: `${ctx.req.user.surname} ${ctx.req.user.name} (${ctx.req.user.email})`,
            content: obj
          });
        };
      });
    }
  };
};
