"use strict";

const mailer = require("../../../plugins/nodemailer/mailer");
const { sanitizeEntity } = require("strapi-utils");
const service = require("../services/mail-template");
const bcrypt = require("bcrypt");
const fs = require("fs");
const { validateParams } = require("../services/mail-template");
const { gen_password } = require("../../invite-user/services/invite-user");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async find(ctx) {
    let groups = [];
    let result = [];

    const entites = await strapi.services["mail-template"].find();
    const groups_req = await strapi.services["mail-group"].find({
      userId: ctx.query.userId,
    });

    groups_req.map((group) => {
      groups.push({
        id: group.id,
      });
    });

    entites.map((entity) => {
      let status = false;
      delete entity.created_by;
      delete entity.published_at;
      delete entity.updated_by;
      delete entity.created_at;
      delete entity.updated_at;
      delete entity.roles;
      entity.group_id.map((y) => {
        groups.map((i) => {
          if (y.id === i.id) {
            status = true;
          }
        });
        if ((y.name = "default")) {
          status = true;
          entity.default = true;
        }
        delete y.published_at;
        delete y.created_by;
        delete y.updated_by;
        delete y.created_at;
        delete y.updated_at;
      });
      delete entity.group_id;

      if (status) {
        result.push(entity);
      }
    });

    return await ctx.send(result);
  },
  async findOne(ctx) {
    const { id } = ctx.params;

    const rules = service.getAllParams();

    const entity = await strapi.services["mail-template"].findOne({ id });
    if (!entity) {
      return ctx.send({ message: "Неверный ID у шаблона" });
    }

    let arr = [];

    rules.forEach((y) => {
      if (entity.html.indexOf(y) > 0) {
        arr.push(y);
      }
    });

    delete entity.created_at;
    delete entity.updated_at;
    delete entity.published_at;
    delete entity.roles;
    entity.params = arr;

    return sanitizeEntity(entity, { model: strapi.models["mail-template"] });
  },
  async sendMail(ctx) {
    const id = ctx.params.id;

    const email = ctx.query.email;

    let body = ctx.request.body;
    console.log("Body: ", body);

    if (!email) {
      return ctx.send("Не указана почта для отправления сообщения!");
    }

    let result = await strapi.services["mail-template"].find({ id: id });
    result = {
      id: result[0].id,
      subject: result[0].subject,
      html: result[0].html,
    };

    let { status, updHtml } = service.validateParams(result.html, ctx.query);
    if (!status) {
      return ctx.send("Ошибка!");
    }

    try {
      await mailer(null, {
        to: email,
        subject: result.subject,
        html: updHtml,
      });
    } catch (e) {
      console.error(e);
    }

    return ctx.send({
      status: true,
      id: id,
      message: result,
      updHtml,
    });
  },
  async getUserGroups(ctx) {
    const { id } = ctx.params;
    let entites = [];

    const query = await strapi.services["mail-group"].find({ userId: id });
    query.map((y) => {
      let permission = [];
      let templates = [];

      y.permission_id.map((i) => {
        permission.push({
          id: i.id,
          rule: i.rule,
        });
      });
      y.template_id.map((i) => {
        templates.push({
          id: i.id,
          subject: i.subject,
          html: i.html,
        });
      });

      entites.push({
        id: y.id,
        name: y.name,
        permission,
        templates,
      });
    });

    return ctx.send(entites);
  },
  async passwordMailTemplate(ctx) {
    const emails = ["@ren-tv.com", "@iz.ru"];
    const temp_mail = [
      "y.antonov@iz.ru",
      "b.moshnin@iz.ru",
      "t.skandari@iz.ru",
      "i.chernyak@iz.ru",
      "s.kabanov@iz.ru",
    ];

    const req = await strapi.plugins[
      "users-permissions"
    ].services.user.fetchAll({
      _limit: 500,
    });
    let users = [];
    let mails = [];
    let temp_mails = [];
    let count = 0;
    req.map((user) => {
      let clearMail = user.email;
      emails.map((email) => {
        clearMail = clearMail.replace(new RegExp(email, "gi"), "");
      });
      users.push({
        name: user.name,
        email: user.email,
        password: clearMail,
        checkPassword: user.password,
      });
    });

    users.map(async (user) => {
      if (bcrypt.compareSync(user.password, user.checkPassword)) {
        mails.push(user);
        count += 1;

        temp_mail.forEach(async (check) => {
          if (check === user.email) {
            temp_mails.push(user);

            const password = gen_password(8);

            await strapi.query("user", "users-permissions").update(
              { email: user.email },
              {
                resetPasswordToken: null,
                password: bcrypt.hashSync(password, 10),
              }
            );

        try{
	        mailer(null, {
	            from: {name: "Администратор системы plan.iz.ru", address: "<no-reply@iz.ru>"},
              to: user.email,
              subject: "Вам необходимо сменить пароль от аккаунта!",
              html: `<p>Добрый день, ${user.name}!</p>
              <p>В связи с проведением работ по усилению мер безопасности в системе Планировщика, все старые простые пароли были автоматически изменены на более надежные.</p>
                   <p> Пожалуйста, используйте для входа новые данные: </p>
                    Логин: <strong>${user.email}</strong> <br />
                    Пароль: <strong>${password}</strong> <br />
                    Ссылка для входа: <a href="${process.env.FRONTEND_API}login">${process.env.FRONTEND_API}login</a> <br />
                    Спасибо.
                    `,
            });}
            catch(e){
              console.log(e)
            }
          }
        });
      }
    });

    return ctx.send({ count, mails, temp_mails });
  },
};
