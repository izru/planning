"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

const arrParams = [
  "%%USERNAME%%",
  "%%USERSURNAME%%",
  "%%USEREMAIL%%",
  "%%USERCODE%%",
  "%%USERMOBILE%%",
];

module.exports = {
  getAllParams() {
    return arrParams;
  },
  validateParams(html, params) {
    let arr = [];
    arrParams.forEach((y) => {
      if (html.indexOf(y) >= 0) {
        if (!params[y]) {
          return { status: false, error: "Нет определённого поля" };
        }
        arr.push({ param: y, result: params[y] });
      }
    });

    arr.forEach((y) => {
      let re = new RegExp(y.param, "gi");
      html = html.replace(re, y.result);
    });

    return { status: true, params: arr, updHtml: html };
  },
};
