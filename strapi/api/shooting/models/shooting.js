'use strict';
const _ = require("lodash");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

 module.exports = {
  lifecycles: {
    async beforeUpdate(params, data) {
      let contentBeforeUpdate = await strapi.query('shooting').findOne({id: params.id});
      data.contentBeforeUpdate = contentBeforeUpdate;
    },
    async afterUpdate(result, params, data) {
      delete data.contentBeforeUpdate.auto_create;
      delete data.contentBeforeUpdate.urgent_departure;
      delete data.contentBeforeUpdate.author;
      delete data.contentBeforeUpdate.shootings_parent;
      delete data.contentBeforeUpdate.shootings_child;
      delete data.contentBeforeUpdate.created_by;
      delete data.contentBeforeUpdate.updated_by;
      delete data.contentBeforeUpdate.created_at;
      delete data.contentBeforeUpdate.updated_at;
      delete result.auto_create;
      delete result.urgent_departure;
      delete result.author;
      delete result.shootings_parent;
      delete result.shootings_child;
      delete result.created_by;
      delete result.updated_by;
      delete result.created_at;
      delete result.updated_at;

      const diff = function(obj1, obj2) {
        return _.reduce(obj1, function(result, value, key) {
          if (_.isPlainObject(value)) {
            result[key] = diff(value, obj2[key]);
          } else if (!_.isEqual(value, obj2[key])) {
            result[key] = value;
          }
          return result;
        }, {});
      };

      let id = result.id;

      // создание записи в истории изменения съемки или срочного выезда
      if (data._state && !result.personal && !result.duty_group && !result.coordination_event) {
        delete data.contentBeforeUpdate.personalType;
        delete data.contentBeforeUpdate.personal;
        delete data.contentBeforeUpdate.duty_group;
        delete data.contentBeforeUpdate.coordination_event;
        delete result.personalType;
        delete result.personal;
        delete result.duty_group;
        delete result.coordination_event;

        let name = result.name || '';

        let before_metatheme_inclusions = [];
        data.contentBeforeUpdate.metatheme_inclusions.every(i => before_metatheme_inclusions.push(i.name));
        data.contentBeforeUpdate.metatheme_inclusions = before_metatheme_inclusions;
        let before_metatheme_aether_plans = [];
        data.contentBeforeUpdate.metatheme_aether_plans.every(ap => before_metatheme_aether_plans.push(ap.name));
        data.contentBeforeUpdate.metatheme_aether_plans = before_metatheme_aether_plans;
        let before_metatheme_hardwares = [];
        data.contentBeforeUpdate.metatheme_hardwares.every(h => before_metatheme_hardwares.push(h.name));
        data.contentBeforeUpdate.metatheme_hardwares = before_metatheme_hardwares;
        let before_tech_resources = [];
        data.contentBeforeUpdate.tech_resources.every(tr => before_tech_resources.push(tr.name));
        data.contentBeforeUpdate.tech_resources = before_tech_resources;
        let before_correspondents = [];
        let before_shooting_group = [];
        let before_drivers = [];
        for (let i = 0; i < data.contentBeforeUpdate.employees.length; i++) {
          let surname = data.contentBeforeUpdate.employees[i].surname ? data.contentBeforeUpdate.employees[i].surname : '';
          let name = data.contentBeforeUpdate.employees[i].name ? ` ${data.contentBeforeUpdate.employees[i].name.charAt(0)}.` : '';
          let patronymic = data.contentBeforeUpdate.employees[i].patronymic ? `${data.contentBeforeUpdate.employees[i].patronymic.charAt(0)}.` : '';
          if (data.contentBeforeUpdate.employees[i].employee_role === 4 || data.contentBeforeUpdate.employees[i].employee_role === 7 ||
              data.contentBeforeUpdate.employees[i].employee_role === 8 || data.contentBeforeUpdate.employees[i].employee_role === 15 ||
              data.contentBeforeUpdate.employees[i].employee_role === 16 || data.contentBeforeUpdate.employees[i].employee_role === 18 ||
              data.contentBeforeUpdate.employees[i].employee_role === 19)
          {
            before_correspondents.push(`${surname}${name}${patronymic}`);
          } else if (data.contentBeforeUpdate.employees[i].employee_role === 1 || data.contentBeforeUpdate.employees[i].employee_role === 5 ||
                     data.contentBeforeUpdate.employees[i].employee_role === 12)
          {
            before_shooting_group.push(`${surname}${name}${patronymic}`);
          } else if (data.contentBeforeUpdate.employees[i].employee_role === 2 || data.contentBeforeUpdate.employees[i].employee_role === 20)
          {
            before_drivers.push(`${surname}${name}${patronymic}`);
          }
        };
        data.contentBeforeUpdate.correspondents = before_correspondents;
        data.contentBeforeUpdate.shooting_group = before_shooting_group;
        data.contentBeforeUpdate.drivers = before_drivers;

        let result_metatheme_inclusions = [];
        result.metatheme_inclusions.every(i => result_metatheme_inclusions.push(i.name));
        result.metatheme_inclusions = result_metatheme_inclusions;
        let result_metatheme_aether_plans = [];
        result.metatheme_aether_plans.every(ap => result_metatheme_aether_plans.push(ap.name));
        result.metatheme_aether_plans = result_metatheme_aether_plans;
        let result_metatheme_hardwares = [];
        result.metatheme_hardwares.every(h => result_metatheme_hardwares.push(h.name));
        result.metatheme_hardwares = result_metatheme_hardwares;
        let result_tech_resources = [];
        result.tech_resources.every(tr => result_tech_resources.push(tr.name));
        result.tech_resources = result_tech_resources;
        let result_correspondents = [];
        let result_shooting_group = [];
        let result_drivers = [];
        for (let i = 0; i < result.employees.length; i++) {
          let surname = result.employees[i].surname ? result.employees[i].surname : '';
          let name = result.employees[i].name ? ` ${result.employees[i].name.charAt(0)}.` : '';
          let patronymic = result.employees[i].patronymic ? `${result.employees[i].patronymic.charAt(0)}.` : '';
          if (result.employees[i].employee_role === 4 || result.employees[i].employee_role === 7 ||
              result.employees[i].employee_role === 8 || result.employees[i].employee_role === 15 ||
              result.employees[i].employee_role === 16 || result.employees[i].employee_role === 18 ||
              result.employees[i].employee_role === 19)
          {
            result_correspondents.push(`${surname}${name}${patronymic}`);
          } else if (result.employees[i].employee_role === 1 || result.employees[i].employee_role === 5 ||
            result.employees[i].employee_role === 12)
          {
            result_shooting_group.push(`${surname}${name}${patronymic}`);
          } else if (result.employees[i].employee_role === 2 || result.employees[i].employee_role === 20)
          {
            result_drivers.push(`${surname}${name}${patronymic}`);
          }
        };
        result.correspondents = result_correspondents;
        result.shooting_group = result_shooting_group;
        result.drivers = result_drivers;

        delete data.contentBeforeUpdate.employees;
        delete result.employees;
        let resultCopy = JSON.parse(JSON.stringify(result));
        let dataContentBeforeUpdateCopy = JSON.parse(JSON.stringify(data.contentBeforeUpdate));
        let contentBefore = diff(dataContentBeforeUpdateCopy, resultCopy);
        let content = diff(result, data.contentBeforeUpdate);

        let themeId = result.metatheme ? result.metatheme.id : null
        delete contentBefore.metatheme;
        delete content.metatheme;

        if (Object.keys(contentBefore).length !== 0 && Object.keys(content).length !== 0) {
          await strapi.services.historyrecord.create({
            contentId: id,
            contentType: 'Съемка',
            name: name,
            action: 'update',
            author: `${data._state.user.surname} ${data._state.user.name} (${data._state.user.email})`,
            content: content,
            contentBefore: contentBefore
          })
        };

        if (themeId && (content.correspondents || content.shooting_group || content.drivers || content.tech_resources)) {
          for (var key in content) {
            if (key !== 'correspondents' && key !== 'shooting_group' && key !== 'drivers' && key !== 'tech_resources') {
              delete content.key;
              delete contentBefore.key;
            }
          };
          strapi.services.historyrecord.create({
            contentId: themeId,
            contentType: 'Тема',
            name: name,
            action: 'update',
            author: `${data._state.user.surname} ${data._state.user.name} (${data._state.user.email})`,
            content: content,
            contentBefore: contentBefore
          })
        };
      }

      // создание записи в истории изменения в графике сотрудника (Событие сотрудника)
      else if (data._state && result.personal) {
        delete data.contentBeforeUpdate.duty_group;
        delete data.contentBeforeUpdate.coordination_event;
        delete data.contentBeforeUpdate.personal;
        delete data.contentBeforeUpdate.reserved;
        delete data.contentBeforeUpdate.date_departure;
        delete data.contentBeforeUpdate.date_arrival;
        delete data.contentBeforeUpdate.date_return;
        delete data.contentBeforeUpdate.address;
        delete data.contentBeforeUpdate.comment;
        delete data.contentBeforeUpdate.comment_car;
        delete data.contentBeforeUpdate.comment_format;
        delete data.contentBeforeUpdate.comment_tech;
        delete data.contentBeforeUpdate.comment_aether_plans;
        delete data.contentBeforeUpdate.comment_inclusions;
        delete data.contentBeforeUpdate.metatheme;
        delete data.contentBeforeUpdate.metatheme_aether_plans;
        delete data.contentBeforeUpdate.metatheme_hardwares;
        delete data.contentBeforeUpdate.metatheme_inclusions;
        delete data.contentBeforeUpdate.tech_resources;
        delete data.contentBeforeUpdate.status_coord;
        delete result.duty_group;
        delete result.coordination_event;
        delete result.personal;
        delete result.reserved;
        delete result.date_departure;
        delete result.date_arrival;
        delete result.date_return;
        delete result.address;
        delete result.comment;
        delete result.comment_car;
        delete result.comment_format;
        delete result.comment_tech;
        delete result.comment_aether_plans;
        delete result.comment_inclusions;
        delete result.metatheme;
        delete result.metatheme_aether_plans;
        delete result.metatheme_hardwares;
        delete result.metatheme_inclusions;
        delete result.tech_resources;
        delete result.status_coord;

        if (result.employees.length) {
          data.contentBeforeUpdate.comment = result.employees[0].comment_calendar ? result.employees[0].comment_calendar : '';
          result.comment = data.comment_calendar ? data.comment_calendar : '';
        }

        delete data.contentBeforeUpdate.employees;
        delete result.employees;

        let name = result.personalType || '';

        let resultCopy = JSON.parse(JSON.stringify(result));
        let dataContentBeforeUpdateCopy = JSON.parse(JSON.stringify(data.contentBeforeUpdate));
        let contentBefore = diff(dataContentBeforeUpdateCopy, resultCopy);
        let content = diff(result, data.contentBeforeUpdate);

        if (Object.keys(contentBefore).length !== 0 && Object.keys(content).length !== 0) {
          await strapi.services.historyrecord.create({
            contentId: id,
            contentType: 'Событие сотрудника',
            name: name,
            action: 'update',
            author: `${data._state.user.surname} ${data._state.user.name} (${data._state.user.email})`,
            content: content,
            contentBefore: contentBefore
          })
        };
      }
    }
  }
};
