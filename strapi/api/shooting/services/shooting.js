'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

 module.exports = {
  find(params, populate) {
    return strapi.query('shooting').find(params, ["metatheme_aether_plans", "metatheme_hardwares", "metatheme_inclusions", "metatheme", "tech_resources", "shootings_child", "shootings_child.employees", "shootings_parent", "shootings_parent.employees", "employees"]);
  },
  findOne(params, populate) {
    return strapi.query('shooting').findOne(params, ["metatheme_aether_plans", "metatheme_hardwares", "metatheme_inclusions", "metatheme", "tech_resources", "shootings_child", "shootings_child.employees", "shootings_parent", "shootings_parent.employees", "employees"]);
  }
};
