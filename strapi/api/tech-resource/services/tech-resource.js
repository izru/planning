'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

 module.exports = {
  find(params, populate) {
    return strapi.query('tech-resource').find(params, ["status", "location"]);
  },
  findOne(params, populate) {
    return strapi.query('tech-resource').findOne(params, ["status", "location"]);
  }
};
