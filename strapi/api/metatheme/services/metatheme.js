'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  find(params, populate) {
    return strapi.query('metatheme').find(params, ["metatheme_aether_plans", "metatheme_aethers", "metatheme_hardwares", "metatheme_inclusions", "metatheme_section", "shootings", "shootings.employees", "shootings.shootings_parent", "shootings.shootings_child", "shootings.metatheme_aether_plans", "shootings.tech_resources", "employees"]);
  },
  findOne(params, populate) {
    return strapi.query('metatheme').findOne(params, ["metatheme_aether_plans", "metatheme_aethers", "metatheme_hardwares", "metatheme_inclusions", "metatheme_section", "shootings", "shootings.employees", "shootings.shootings_parent", "shootings.shootings_child", "shootings.metatheme_aether_plans", "shootings.tech_resources", "employees"]);
  }
};
