'use strict';
const _ = require("lodash");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

 module.exports = {
  lifecycles: {
    async beforeUpdate(params, data) {
      let contentBeforeUpdate = await strapi.query('metatheme').findOne({id: params.id});
      data.contentBeforeUpdate = contentBeforeUpdate;
    },
    async afterUpdate(result, params, data) {
      if (data._state) {
        if (data.published_at === null) {
          let obj = {
            id: result.id,
            name: result.name,
            date_start: result.date_start,
            date_end: result.date_end,
            metatheme_section: result.metatheme_section.name
          }
          strapi.services.historyrecord.create({
            contentId: result.id,
            contentType: 'Тема',
            name: result.name || '',
            action: 'delete',
            author: `${data._state.user.surname} ${data._state.user.name} (${data._state.user.email})`,
            content: obj
          })
        } else {
          delete data.contentBeforeUpdate.shootings;
          delete data.contentBeforeUpdate.sortParam;
          delete data.contentBeforeUpdate.sortAetherPlans;
          delete data.contentBeforeUpdate.sortTomorrowPlans;
          delete data.contentBeforeUpdate.author;
          delete data.contentBeforeUpdate.created_by;
          delete data.contentBeforeUpdate.updated_by;
          delete data.contentBeforeUpdate.created_at;
          delete data.contentBeforeUpdate.updated_at;
          delete data.contentBeforeUpdate.published_at;
          delete result.shootings;
          delete result.sortParam;
          delete result.sortAetherPlans;
          delete result.sortTomorrowPlans;
          delete result.author;
          delete result.created_by;
          delete result.updated_by;
          delete result.created_at;
          delete result.updated_at;
          delete result.published_at;

          if (data.contentBeforeUpdate.short_description) {
            data.contentBeforeUpdate.short_description = data.contentBeforeUpdate.short_description.replace(/\ufeff/gi, '');
          };
          if (result.short_description) {
            result.short_description = result.short_description.replace(/\ufeff/gi, '');
          };
          if (data.contentBeforeUpdate.description) {
            data.contentBeforeUpdate.description = data.contentBeforeUpdate.description.replace(/\ufeff/gi, '');
          };
          if (result.description) {
            result.description = result.description.replace(/\ufeff/gi, '');
          };

          data.contentBeforeUpdate.aether_plan_checkboxes = [];
          if (data.contentBeforeUpdate.aether_plan_5tv) {
            data.contentBeforeUpdate.aether_plan_checkboxes.push('5TV')
          };
          if (data.contentBeforeUpdate.aether_plan_iztv) {
            data.contentBeforeUpdate.aether_plan_checkboxes.push('IZ.TV')
          };
          if (data.contentBeforeUpdate.aether_plan_rentv) {
            data.contentBeforeUpdate.aether_plan_checkboxes.push('РЕН ТВ')
          };
          result.aether_plan_checkboxes = [];
          if (result.aether_plan_5tv) {
            result.aether_plan_checkboxes.push('5TV')
          };
          if (result.aether_plan_iztv) {
            result.aether_plan_checkboxes.push('IZ.TV')
          };
          if (result.aether_plan_rentv) {
            result.aether_plan_checkboxes.push('РЕН ТВ')
          };
          delete data.contentBeforeUpdate.aether_plan_5tv;
          delete data.contentBeforeUpdate.aether_plan_iztv;
          delete data.contentBeforeUpdate.aether_plan_rentv;
          delete result.aether_plan_5tv;
          delete result.aether_plan_iztv;
          delete result.aether_plan_rentv;

          const diff = function(obj1, obj2) {
            return _.reduce(obj1, function(result, value, key) {
              if (_.isPlainObject(value)) {
                result[key] = diff(value, obj2[key]);
              } else if (!_.isEqual(value, obj2[key])) {
                result[key] = value;
              }
              return result;
            }, {});
          };

          let id = result.id;
          let name = result.name || '';

          data.contentBeforeUpdate.metatheme_section = data.contentBeforeUpdate.metatheme_section.name;
          let before_metatheme_inclusions = [];
          data.contentBeforeUpdate.metatheme_inclusions.every(i => before_metatheme_inclusions.push(i.name));
          data.contentBeforeUpdate.metatheme_inclusions = before_metatheme_inclusions;
          let before_metatheme_aethers = [];
          data.contentBeforeUpdate.metatheme_aethers.every(a => before_metatheme_aethers.push(a.name));
          data.contentBeforeUpdate.metatheme_aethers = before_metatheme_aethers;
          let before_metatheme_aether_plans = [];
          data.contentBeforeUpdate.metatheme_aether_plans.every(ap => before_metatheme_aether_plans.push(ap.name));
          data.contentBeforeUpdate.metatheme_aether_plans = before_metatheme_aether_plans;
          let before_metatheme_hardwares = [];
          data.contentBeforeUpdate.metatheme_hardwares.every(h => before_metatheme_hardwares.push(h.name));
          data.contentBeforeUpdate.metatheme_hardwares = before_metatheme_hardwares;

          result.metatheme_section = result.metatheme_section.name;
          let result_metatheme_inclusions = [];
          result.metatheme_inclusions.every(i => result_metatheme_inclusions.push(i.name));
          result.metatheme_inclusions = result_metatheme_inclusions;
          let result_metatheme_aethers = [];
          result.metatheme_aethers.every(a => result_metatheme_aethers.push(a.name));
          result.metatheme_aethers = result_metatheme_aethers;
          let result_metatheme_aether_plans = [];
          result.metatheme_aether_plans.every(ap => result_metatheme_aether_plans.push(ap.name));
          result.metatheme_aether_plans = result_metatheme_aether_plans;
          let result_metatheme_hardwares = [];
          result.metatheme_hardwares.every(h => result_metatheme_hardwares.push(h.name));
          result.metatheme_hardwares = result_metatheme_hardwares;

          let resultCopy = JSON.parse(JSON.stringify(result));
          let dataContentBeforeUpdateCopy = JSON.parse(JSON.stringify(data.contentBeforeUpdate));
          let contentBefore = diff(dataContentBeforeUpdateCopy, resultCopy);
          let content = diff(result, data.contentBeforeUpdate);

          if (Object.keys(contentBefore).length !== 0 && Object.keys(content).length !== 0) {
            strapi.services.historyrecord.create({
              contentId: id,
              contentType: 'Тема',
              name: name,
              action: 'update',
              author: `${data._state.user.surname} ${data._state.user.name} (${data._state.user.email})`,
              content: content,
              contentBefore: contentBefore
            })

            strapi.StrapIO.emit({identity: 'Metatheme'}, 'update', {id: id});
          }
        }
      }
    }
  }
};
