"use strict";

const jwt = require("jsonwebtoken");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

const symbols =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

const arr = ["1h", "10h", "24h", "48h"];

class InviteService {
  async checkUser(email) {
    let check = await strapi
      .query("user", "users-permissions")
      .find({ email: email });
    if (check.length > 0) {
      return true;
    }
    return false;
  }

  async checkInvite(email) {
    let check = await strapi.services["invite-user"].find({ EMAIl: email });
    if (check.length > 0) {
      return true;
    }
    return false;
  }

  gen_password(len) {
    var password = "";
    for (var i = 0; i < len; i++) {
      password += symbols.charAt(Math.floor(Math.random() * symbols.length));
    }
    return password;
  }

  gen_jwt(key, time) {
    const exp = arr.find((y) => y === time) || "1h";

    return jwt.sign({}, key || process.env.MAIL_JWT_KEY, {
      expiresIn: exp,
    });
  }
}

module.exports = new InviteService();
