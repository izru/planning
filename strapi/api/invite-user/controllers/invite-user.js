const jwt = require("jsonwebtoken");
const uuid = require("uuid");
const mailer = require("../../../plugins/nodemailer/mailer");
const InviteService = require("../services/invite-user");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  create: async (ctx) => {
    console.log("---- Called invite-user.create ----");

    let body = ctx.request.body;
    console.log("Body: ", body);

    let check = await InviteService.checkUser(body.EMAIL);
    if (check) {
      return await ctx.send({
        status: false,
        message: "Данная почта уже зарегистрирована в Планировщик",
      });
    }
    check = await InviteService.checkInvite(body.EMAIL);
    if (check) {
      return await ctx.send({
        status: false,
        message:
          "Пользователь с данной почтой уже имеет приглашение в Планировщик",
      });
    }

    let obj = {
      EMAIl: body.EMAIL,
      NAME: body.NAME,
      SURNAME: body.SURNAME,
      EXPIRED: InviteService.gen_jwt("sample_key", body.exp),
      PASSWORD: InviteService.gen_password(6),
      UUID: uuid.v4(),
    };

    let sendOpt = {
      to: body.EMAIL,
      subject: `${obj.NAME}, Вас пригласили в Планировщик`,
      html: `
                <p>Пройдите по специальной ссылке, указанной ниже, чтобы подтвердить свой аккаунт и проверить данные.</p>
                <a href="${process.env.FRONTEND_API}invite/${obj.UUID}">Ваша ссылка на подтверждение аккаунта</a>
            `,
    };

    return await strapi.services["invite-user"].create(obj).then(async () => {
      try {
        await mailer(null, sendOpt);
      } catch (e) {
        console.log(e);
        return await ctx.send({ ...obj, status: false });
      } finally {
        return await ctx.send({ ...obj, status: true });
      }
    });
  },

  update: async (ctx) => {
    console.log("---- Called invite-user.update ----");

    let data = Math.floor(Date.parse(ctx.request.body.date) / 1000);
    let req = ctx.params.id;

    let result = await strapi.services["invite-user"].find({ id: req });
    result = result[0];

    let exp = jwt.decode(result.EXPIRED);
    if (exp <= data) {
      await strapi.services["invite-user"].delete({ id: req });
      throw Error({ message: "Приглашение устарело" });
    }

    return await strapi.plugins["users-permissions"].services.user
      .add({
        username: result["EMAIl"],
        surname: result["SURNAME"],
        email: result["EMAIl"],
        password: result["PASSWORD"],
        name: result["NAME"],
        blocked: false,
        confirmed: true,
        provider: "local",
        role: 1,
      })
      .then(async (data) => {
        await strapi.services["invite-user"]
          .delete({ id: req })
          .then((data) => {
            return { status: true };
          });
      })
      .then(async () => {
        console.log(result);
        await mailer(null, {
          to: result["EMAIl"],
          subject:
            "Ваш аккаунт в системе Планирования (plan.iz.ru) подтверждён",
          html: `
          <p>Ваш логин: <strong>${result["EMAIl"]}</strong></p> 
          <p>Ваш пароль: <strong>${result["PASSWORD"]}</strong></p> 
          <a href="${process.env.FRONTEND_API}login">Авторизуйтесь здесь</a>
          `,
        }).then((data) => console.log(data.messageId));
      });
  },

  refresh: async (ctx) => {
    const email = ctx.params.email;

    const body = ctx.request.body;

    console.log(body);

    let check = await strapi.services["invite-user"].find({ EMAIl: email });
    if (!check) {
      throw Error({ message: "Неверное приглашение" });
    }

    let jwt = InviteService.gen_jwt("sample_key", body.exp);

    const result = await strapi.services["invite-user"].update(
      { EMAIl: email },
      { EXPIRED: jwt }
    );

    console.log(check);

    check = check[0];

    await mailer(null, {
      to: email,
      subject: `${check.NAME}, Вас пригласили в Планировщик`,
      html: `
                  <p>Пройдите по специальной ссылке, указанной ниже, чтобы подтвердить свой аккаунт и проверить данные.</p>
                  <a href="${process.env.FRONTEND_API}invite/${check.UUID}">Ваша ссылка на подтверждение аккаунта</a>
              `,
    }).then((data) => {
      console.log(data);
    });

    return await ctx.send(result);
  },
};
