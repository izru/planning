'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

 module.exports = {
  find(params, populate) {
    return strapi.query('employee').find(params, ["employee_role", "location"]);
  },
  findOne(params, populate) {
    return strapi.query('employee').findOne(params, ["employee_role", "location"]);
  }
};
