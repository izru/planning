'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterDelete(result, params) {
      for(let i = 0; i < result.shootings.length; i++) {
        if (result.shootings[i].personal === true) {
          strapi.query('shooting').delete({id: result.shootings[i].id});
        }
      }
    }
  }
};
