let public
if (process.env.VUE_APP_NODE_ENV === "production") {
  public = "plan.iz.ru"
} else if (process.env.VUE_APP_NODE_ENV === "devserver") {
  public = "10.27.1.61:8080"
} else {
  public = "localhost:8080"
}
module.exports = {
  devServer: {
    public: public,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
    },
  },
  pwa: {
    name: "PLANNING",
    themeColor: "#7C43BA",
    icons: {
      favicon32: "img/icons/favicon-32x32.png",
      favicon16: "img/icons/favicon-16x16.png",
      appleTouchIcon: "img/icons/apple-touch-icon-152x152.png",
      maskIcon: "img/icons/safari-pinned-tab.svg",
      msTileImage: "img/icons/msapplication-icon-144x144.png",
    },
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "src/service-worker.js",
    },
  },
};
