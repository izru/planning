import qs from 'qs'

export default {
  data() {
    return {
      watcher: null,
      items: [],
      itemsCount: null,
      title: null
    }
  },
  watch: {
    '$store.state.userRole': {
      immediate: true,
      handler: async function () {
        switch (this.$store.state.userRole) {
          case 'Coordination':
          case 'Accreditation':
            this.watchItems()
            break
          default:
            clearInterval(this.watcher)
            this.watcher = null
        }
      }
    },
    itemsCount: async function (newValue, oldValue) {
      if (oldValue === null) return

      if (newValue > oldValue) {
        let diff = newValue - oldValue
        let params = {
          _limit: diff,
          _sort: 'updated_at:desc'
        }
        let query = ''
        if (this.$store.state.userRole === 'Coordination') {
          query = qs.stringify({ 
            _where: {
              _or: [[
                { 'personal_ne': true },
                { 'status_coord': 'coord' }
              ]]
            }
          })
          this.items = await this.$store.dispatch('fetchShootings', {query, params})
          for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].urgent_departure === true) {
              this.title = 'Добавлен срочный выезд!'
              this.showNotification(this.items[i])
            } else {
              this.title = 'Добавлена съемка!'
              this.showNotification(this.items[i])
            }
          }
        } else if (this.$store.state.userRole === 'Accreditation') {
          query = qs.stringify({ 
            _where: {
              _or: [[
                { 'status_accr': 'need' }
              ]]
            }
          })
          this.items = await this.$store.dispatch('fetchMetathemes', {query, params})
          for (let i = 0; i < this.items.length; i++) {
            this.title = 'Создан запрос на аккредитацию!'
            this.showNotification(this.items[i])
          }
        }
      }
    }
  },
  methods: {
    watchItems() {
      this.watcher = setInterval(async () => {
        let params = {
          _limit: -1,
        }
        let query = ''
        switch (this.$store.state.userRole) {
          case 'Coordination':
            query = qs.stringify({ 
              _where: {
                _or: [[
                  { 'personal_ne': true },
                  { 'status_coord': 'coord' }
                ]]
              }
            })
            this.itemsCount = await this.$store.dispatch('fetchShootingsCount', {query, params})
            break
          case 'Accreditation':
            query = qs.stringify({ 
              _where: {
                _or: [[
                  { 'status_accr': 'need' }
                ]]
              }
            })
            this.itemsCount = await this.$store.dispatch('fetchMetathemesCount', {query, params})
            break
        }
      }, 3000)
    },
    showNotification(item) {
      const favicon = document.querySelector("link[rel~='icon']")
      let params
      let counter = 1
      const dynamicFaviconTitle = setInterval(() => {
        if (counter == 1) {
          document.title = this.title
          favicon.href = '/img/icons/favicon_warning.png'
          counter = 2
        } else {
          params = Object.values(this.$route.params)
          document.title = `${this.$route.meta.title} ${params.length ? params[0]: ''}`
          favicon.href = '/img/icons/favicon-32x32.png'
          counter = 1
        }
      }, 500)
      let message
      switch (this.$store.state.userRole) {
        case 'Coordination':
          message = this.$createElement('div', {
            style: 'cursor: pointer; text-decoration: underline;',
            on: {
                click: () => {
                  this.$store.commit('setDateCoordination', this.moment(item.date_start).format())
                  this.$store.commit('coordinationUpdated')
                  if (this.$route.name !== 'coordination') {
                    this.$router.push({name: 'coordination'}).catch(()=>{})
                  }
                  setTimeout(() => {
                    const el = document.getElementById(item.id)
                    el.scrollIntoView({block: 'center', behavior: 'smooth'})
                  }, 1000)
                }
              }
            },
            `${this.moment(item.date_start).format('DD.MM.YYYY')} ${this.moment(item.date_start).format('HH:mm')} - ${item.name}`)
          break
        case 'Accreditation':
          message = this.$createElement('div', {},
              [
                this.$createElement('p', {
                  style: 'cursor: pointer; text-decoration: underline;',
                  on: {
                    click: () => {
                      if (this.$route.name !== 'accreditation') {
                        this.$router.push({name: 'accreditation'}).catch(()=>{})
                      }
                      this.$store.commit('setAccreditationThemeId', item.id)
                      setTimeout(() => {
                        this.$store.commit('setAccreditationThemeId', null)
                      }, 2000)
                    }
                  }
                }, `${item.name}`),
                this.$createElement('p', {
                  domProps: {
                    innerHTML:`<span>${item.date_start_accr ? `c ${this.moment(item.date_start_accr).format('DD.MM.YY')} ${this.moment(item.date_start_accr).format('HH:mm')}` : ''}</span>
                              <span>${item.date_end_accr ? `до ${this.moment(item.date_end_accr).format('DD.MM.YY')} ${this.moment(item.date_end_accr).format('HH:mm')}` : ''}</span>`
                    }
                }, [])
              ]
            )
          break
      }
      this.$notify({
        title: this.title,
        dangerouslyUseHTMLString: true,
        message: message,
        type: 'warning',
        duration: 0,
        onClose: () => {
          clearInterval(dynamicFaviconTitle)
          params = Object.values(this.$route.params)
          document.title = `${this.$route.meta.title} ${params.length ? params[0]: ''}`
          favicon.href = '/img/icons/favicon-32x32.png'
        }
      })
    }
  },
  beforeDestroy() {
    clearInterval(this.watcher)
    this.watcher = null
  }
}