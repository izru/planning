export default {
  methods: {
    async exportToPdfMixin(el, title = '', filename = 'output', concatDate = true) {
      let date = `${this.moment(new Date()).format('DD.MM.YYYY')}, ${this.moment(new Date()).format('HH:mm')}`
      let data = new FormData()
      data.append('html', el)
      data.append('title', title)
      data.append('date', date)
      await this.$http({
        method: 'post',
        url: `${this.$store.state.url}/convert/html/to/pdf`,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        data: data,
        responseType: 'arraybuffer'
      })
      .then((response) => {
        let dateForFilename = concatDate ? `__${this.moment(new Date()).format('YYYY-MM-DD_HH-mm')}` : ''
        let blob = new Blob([response.data], { type: 'application/pdf' })
        let link = document.createElement('a')
        link.href = window.URL.createObjectURL(blob)
        link.download = `${filename}${dateForFilename}`
        link.click()
      })
    }
  }
}