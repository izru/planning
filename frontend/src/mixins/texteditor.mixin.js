export default {
  data() {
    return {
      textEditorToolbar: [
        ["bold", "italic", "underline", "strike"],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ color: [] }, { background: [] }],
        [{ align: "" }, { align: "center" }, { align: "right"}, { align: "justify"}]
      ]
    }
  }
}