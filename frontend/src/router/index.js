import Vue from "vue";
import VueRouter from "vue-router";
import moment from "moment";

Vue.use(VueRouter);

const ifAuthenticated = async (to, from, next) => {
  if (
    Vue.cookie.get("token") &&
    Vue.cookie.get("checkToken") &&
    moment(new Date()).diff(Date.parse(Vue.cookie.get("checkToken")), "hours") <
      16
  ) {
    next();
  } else {
    router.push("/login");
  }
};

const routes = [
  {
    path: "/login",
    name: "login",
    meta: { layout: "empty", title: "Вход в систему" },
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/forgotpassword",
    name: "forgotpassword",
    meta: { layout: "empty", title: "Забыли пароль?" },
    component: () => import("../views/ForgotPassword.vue"),
  },
  {
    path: "/resetpassword",
    name: "resetpassword",
    meta: { layout: "empty", title: "Сброс пароля" },
    component: () => import("../views/ResetPassword.vue"),
  },
  {
    path: "/",
    name: "home",
    meta: { layout: "main", title: "Главная" },
    component: () => import("../views/Home.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/profile",
    name: "profile",
    meta: { layout: "main", title: "Личный кабинет" },
    component: () => import("../views/Profile.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/planning",
    name: "planning",
    meta: { layout: "main", title: "Лог" },
    component: () => import("../views/Planning.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/coordination",
    name: "coordination",
    meta: { layout: "main", title: "Координация" },
    component: () => import("../views/Coordination.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/coordination/report",
    name: "coordinationreport",
    meta: { layout: "main", title: "Отчёт: съёмки и сотрудники" },
    component: () => import("../components/coordination/CoordinationReport.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/accreditation",
    name: "accreditation",
    meta: { layout: "main", title: "Аккредитация" },
    component: () => import("../views/Accreditation.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/coordination-needed",
    name: "coordination-needed",
    meta: { layout: "main", title: "Требуется координация" },
    component: () => import("../views/CoordinationNeeded.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/urgentdepartures",
    name: "urgentdepartures",
    meta: { layout: "main", title: "Срочные выезды" },
    component: () => import("../views/UrgentDepartures.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/employeeschedule",
    name: "employeeschedule",
    meta: { layout: "main", title: "График сотрудников" },
    component: () => import("../views/EmployeeSchedule.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/aetherplan",
    name: "aetherplan",
    meta: { layout: "main", title: "Эфирный план" },
    component: () => import("../views/Aetherplan.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/log",
    name: "log",
    meta: { layout: "main", title: "Летучка" },
    component: () => import("../views/Log.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/logfive",
    name: "logfive",
    meta: { layout: "main", title: "Летучка 5 канал" },
    component: () => import("../views/LogFive.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/tomorrowplan",
    name: "tomorrowplan",
    meta: { layout: "main", title: 'Планы МИЦ "Известия на завтра"' },
    component: () => import("../views/Tomorrowplan.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/log/:logDate",
    name: "logsaved",
    meta: { layout: "main", title: "Лог" },
    component: () => import("../components/log/LogSaved.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/logfive/:logDate",
    name: "logfivesaved",
    meta: { layout: "main", title: "Лог 5 канал" },
    component: () => import("../components/logfive/LogFiveSaved.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/employee",
    name: "employee",
    meta: { layout: "main", title: "Управление сотрудниками" },
    component: () => import("../views/Employee.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/employee/:id",
    name: "employeecalendar",
    meta: { layout: "main", title: "Управление сотрудниками" },
    component: () => import("../components/employees/EmployeeCalendar.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/techresources",
    name: "techresources",
    meta: { layout: "main", title: "Управление тех. ресурсами" },
    component: () => import("../views/Techresources.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/invite",
    name: "generateInvite",
    meta: {
      layout: "main",
      title: "Регистрация пользователя в систему Планировщика",
    },
    component: () => import("../views/Invite.vue"),
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/invite/:uuid",
    name: "invite",
    meta: { layout: "main", title: "Приглашение в систему Планировщика " },
    component: () => import("../views/InviteConnect.vue"),
  },
  {
    path: "/mail",
    name: "mail",
    meta: { layout: "main", title: "Шаблоны рассылки" },
    component: () => import("../views/MailTemplate.vue"),
    beforeEnter: ifAuthenticated,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
