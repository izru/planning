import moment from 'moment'

export default {
  state: {
    employeeScheduleDate: moment(new Date()).format(),
    employeeScheduleEvent: 0,
    employeeScheduleQueries: {
      role: 4,
      location: 1
    }
  },
  mutations: {
    setEmployeeScheduleDate (state, payload) {
      state.employeeScheduleDate = moment(payload).format()
    },
    setEmployeeScheduleEvent (state) {
      state.employeeScheduleEvent = state.employeeScheduleEvent + 1
    }
  }
}