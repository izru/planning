import moment from 'moment'

export default {
  state: {
    tomorrowPlanDate: moment(new Date()).set({hour:0,minute:0,second:0,millisecond:0}).format(),
    tomorrowPlanSearchFiled: null
  },
  mutations: {
    setTomorrowPlanDate (state, payload) {
      state.tomorrowPlanDate = moment(payload).set({hour:0,minute:0,second:0,millisecond:0}).format()
    },
    setTomorrowPlanSearchFiled (state, payload) {
      state.tomorrowPlanSearchFiled = payload
    }
  }
}