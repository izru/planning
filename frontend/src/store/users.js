import axios from "axios";

export default {
  state: {
    userName: null,
  },
  actions: {
    async fetchUserById({ commit, dispatch }, id) {
      try {
        const response = await axios.get(`${this.state.url}/users/${id}`);
        return response.data;
      } catch (e) {
        throw e;
      }
    },
    async updateUser({ commit, dispatch }, obj) {
      try {
        return await axios.put(`${this.state.url}/users/${obj.id}`, {
          name: obj.name,
          surname: obj.surname,
          password: obj.password,
        });
      } catch (e) {
        throw e;
      }
    },
    async inviteUser({ commit, dispatch }, obj) {
      try {
        return await axios.post(`${this.state.url}/invite-users`, {
          NAME: obj.NAME,
          SURNAME: obj.SURNAME,
          EMAIL: obj.EMAIL,
          exp: obj.exp,
        });
      } catch (e) {
        throw e;
      }
    },
    async getInvite({ commit, dispatch }, obj) {
      try {
        return await axios.get(
          `${this.state.url}/invite-users?UUID=${obj.uuid}`
        );
      } catch (e) {
        throw e;
      }
    },
    async updateInvite({ commit, dispatch }, obj) {
      try {
        return await axios.put(`${this.state.url}/invite-users/${obj.id}`, {
          date: obj.date,
        });
      } catch (e) {
        throw e;
      }
    },
    async refreshInvite({ commit, dispatch }, obj) {
      try {
        return await axios.put(
          `${this.state.url}/invite-users/refresh/${obj.email}`,
          {
            exp: obj.exp,
          }
        );
      } catch (e) {
        throw e;
      }
    },
    async getTemplates({ commit, dispatch }, obj) {
      try {
        return await axios.get(
          `${this.state.url}/mail-templates/user-groups/${obj.userId}`
        );
      } catch (e) {
        throw e;
      }
    },
    async sendMail({ commit, dispatch }, obj) {
      let str = "";
      obj.body.map((y) => {
        str += `${y.param}=${y.value}&`;
      });
      console.log(str);
      try {
        return await axios.get(
          // Рассылку бека
          `${this.state.url}/mail-templates/send/${obj.mailId}?${str}`
        );
      } catch (e) {
        throw e;
      }
    },
  },
  mutations: {
    setUserName(state, payload) {
      state.userName = payload;
    },
  },
};
