import moment from 'moment'

export default {
  state: {
    aetherPlanDate: moment(new Date()).set({hour:0,minute:0,second:0,millisecond:0}).format(),
    filteredMetathemesAethers: [],
    aetherPlanSearchFiled: null,
    aetherPlanActiveTab: 'rentv'
  },
  mutations: {
    setAetherPlanDate (state, payload) {
      state.aetherPlanDate = moment(payload).set({hour:0,minute:0,second:0,millisecond:0}).format()
    },
    setFilteredMetathemesAethers (state, payload) {
      state.filteredMetathemesAethers = payload
    },
    setAetherPlanSearchFiled (state, payload) {
      state.aetherPlanSearchFiled = payload
    },
    setAetherPlanActiveTab (state, payload) {
      state.aetherPlanActiveTab = payload
    }
  }
}