import axios from 'axios'

export default {
  state: {
    sortShootings: 'date_start:asc'
  },
  actions: {
    async fetchShootings({commit, dispatch}, {query = '', params = {_sort: this.state.shootings.sortShootings}} = {}) {
      try {
        const response = await axios({
          method: 'get',
          url: `${this.state.url}/shootings?${query}`,
          params: params
        })
        return response.data
      } catch (e) {
        throw e
      }
    },
    async fetchShootingsCount({commit, dispatch}, {query = '', params = {_sort: this.state.shootings.sortShootings}} = {}) {
      try {
        const response = await axios({
          method: 'get',
          url: `${this.state.url}/shootings/count?${query}`,
          params: params
        })
        return response.data
      } catch (e) {
        throw e
      }
    },
    async fetchShootingById({commit, dispatch}, id) {
      try {
        const response = await axios.get(`${this.state.url}/shootings/${id}`)
        return response.data
      } catch (e) {
        throw e
      }
    },
    async createShooting({commit, dispatch}, obj) {
      try {
        return await axios.post(
          `${this.state.url}/shootings`,
          {
            name: obj.name,
            metatheme: obj.metatheme,
            reserved: obj.reserved,
            date_departure: obj.date_departure,
            date_arrival: obj.date_arrival,
            date_start: obj.date_start,
            date_end: obj.date_end,
            date_return: obj.date_return,
            address: obj.address,
            metatheme_aether_plans: obj.metatheme_aether_plans,
            comment_aether_plans: obj.comment_aether_plans,
            metatheme_inclusions: obj.metatheme_inclusions,
            comment_inclusions: obj.comment_inclusions,
            metatheme_hardwares: obj.metatheme_hardwares,
            comment: obj.comment,
            comment_format: obj.comment_format,
            comment_tech: obj.comment_tech,
            comment_car: obj.comment_car,
            status_coord: obj.status_coord,
            auto_create: obj.auto_create,
            urgent_departure: obj.urgent_departure,
            employees: obj.employees,
            tech_resources: obj.tech_resources,
            personal: obj.personal,
            personalType: obj.personalType,
            author: obj.author,
            shootings_child: obj.shootings_child,
            shootings_parent: obj.shootings_parent,
            duty_group: obj.duty_group,
            coordination_event: obj.coordination_event
          }
        )
      } catch (e) {
        throw e
      }
    },
    async editShooting({commit, dispatch}, obj) {
      try {
        return await axios.put(
          `${this.state.url}/shootings/${obj.id}`,
          {
            name: obj.name,
            metatheme: obj.metatheme,
            reserved: obj.reserved,
            date_departure: obj.date_departure,
            date_arrival: obj.date_arrival,
            date_start: obj.date_start,
            date_end: obj.date_end,
            date_return: obj.date_return,
            address: obj.address,
            metatheme_aether_plans: obj.metatheme_aether_plans,
            comment_aether_plans: obj.comment_aether_plans,
            metatheme_inclusions: obj.metatheme_inclusions,
            comment_inclusions: obj.comment_inclusions,
            metatheme_hardwares: obj.metatheme_hardwares,
            comment: obj.comment,
            comment_format: obj.comment_format,
            comment_tech: obj.comment_tech,
            comment_car: obj.comment_car,
            status_coord: obj.status_coord,
            employees: obj.employees,
            tech_resources: obj.tech_resources,
            personalType: obj.personalType,
            shootings_child: obj.shootings_child,
            shootings_parent: obj.shootings_parent,
            comment_calendar: obj.comment_calendar
          }
        )
      } catch (e) {
        throw e
      }
    },
    async deleteShooting({commit, dispatch}, id) {
      try {
        return await axios.delete(`${this.state.url}/shootings/${id}`)
      } catch (e) {
        throw e
      }
    }
  },
  mutations: {
    setSortShootings (state, payload) {
      state.sortShootings = payload
    }
  }
}