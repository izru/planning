import router from '../router'
import moment from 'moment'

export default {
  state: {
    dateUrgentDepartures: moment(new Date()).set({hour:0,minute:0,second:0,millisecond:0}).format(),
    rangeUrgentDepartures: 0,
    urgentUpdated: false,
    urgentDeparturesSearchFiled: null
  },
  mutations: {
    setDateUrgentDepartures (state, payload) {
      state.dateUrgentDepartures = moment(payload).set({hour:0,minute:0,second:0,millisecond:0}).format()
      state.rangeUrgentDepartures = 0
      router.push({ query: { date: moment(payload).format('DD-MM-YYYY') }}).catch(()=>{})
    },
    setRangeUrgentDepartures (state, payload) {
      switch (payload) {
        case 'day':
          state.rangeUrgentDepartures = 0
          router.push({ query: { date: moment(state.dateUrgentDepartures).format('DD-MM-YYYY') }}).catch(()=>{})
          break
        case 'week':
          state.rangeUrgentDepartures = 7
          router.push({
            query: {
              date: moment(state.dateUrgentDepartures).format('DD-MM-YYYY'),
              date_finish: moment(state.dateUrgentDepartures).add(state.rangeUrgentDepartures, 'days').format('DD-MM-YYYY')
            }
          }).catch(()=>{})
          break
        case 'month':
          const daysInMonth = moment(state.dateUrgentDepartures).daysInMonth()
          state.rangeUrgentDepartures = daysInMonth
          router.push({
            query: {
              date: moment(state.dateUrgentDepartures).format('DD-MM-YYYY'),
              date_finish: moment(state.dateUrgentDepartures).add(state.rangeUrgentDepartures, 'days').format('DD-MM-YYYY')
            }
          }).catch(()=>{})
          break
      }
    },
    urgentDeparturesUpdated (state) {
      state.urgentUpdated = !state.urgentUpdated
    },
    setUrgentDeparturesSearchFiled (state, payload) {
      state.urgentDeparturesSearchFiled = payload
    }
  }
}