import axios from 'axios'

export default {
  actions: {
    async fetchHistoryRecords({commit, dispatch}, {query = '', params = {_sort: `createdAt:desc`}} = {}) {
      try {
        const response = await axios({
          method: 'get',
          url: `${this.state.url}/historyrecords?${query}`,
          params: params
        })
        return response.data
      } catch (e) {
        throw e
      }
    }
  }
}