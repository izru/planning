import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import users from './users'
import locations from './locations'
import techresources from './techresources'
import metathemes from './metathemes'
import shootings from './shootings'
import log from './log'
import employees from './employees'
import employeeschedule from './employeeschedule'
import urgentdepartures from './urgentdepartures'
import coordination from './coordination'
import aetherplan from './aetherplan'
import tomorrowplan from './tomorrowplan'
import accreditation from './accreditation'
import historyrecords from './historyrecords'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    url: process.env.VUE_APP_URL,
    userRole: null,
    socket: null
  },
  mutations: {
    setUserRole (state, payload) {
      state.userRole = payload
    },
    setSocket (state, payload) {
      state.socket = payload
    }
  },
  actions: {
  },
  getters: {
  },
  modules: {
    auth,
    users,
    locations,
    techresources,
    metathemes,
    shootings,
    log,
    employees,
    employeeschedule,
    urgentdepartures,
    coordination,
    aetherplan,
    tomorrowplan,
    accreditation,
    historyrecords
  }
})
