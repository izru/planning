import router from '../router'
import moment from 'moment'

export default {
  state: {
    dateCoordination: moment(new Date()).set({hour:0,minute:0,second:0,millisecond:0}).format(),
    rangeCoordination: 0,
    coordinationUpdated: false,
    coordinationSearchFiled: null
  },
  mutations: {
    setDateCoordination (state, payload) {
      state.dateCoordination = moment(payload).set({hour:0,minute:0,second:0,millisecond:0}).format()
      state.rangeCoordination = 0
      router.push({ query: { date: moment(payload).format('DD-MM-YYYY') }}).catch(()=>{})
    },
    setRangeCoordination (state, payload) {
      switch (payload) {
        case 'day':
          state.rangeCoordination = 0
          router.push({ query: { date: moment(state.dateCoordination).format('DD-MM-YYYY') }}).catch(()=>{})
          break
        case 'week':
          state.rangeCoordination = 7
          router.push({
            query: {
              date: moment(state.dateCoordination).format('DD-MM-YYYY'),
              date_finish: moment(state.dateCoordination).add(state.rangeCoordination, 'days').format('DD-MM-YYYY')
            }
          }).catch(()=>{})
          break
        case 'month':
          const daysInMonth = moment(state.dateCoordination).daysInMonth()
          state.rangeCoordination = daysInMonth
          router.push({
            query: {
              date: moment(state.dateCoordination).format('DD-MM-YYYY'),
              date_finish: moment(state.dateCoordination).add(state.rangeCoordination, 'days').format('DD-MM-YYYY')
            }
          }).catch(()=>{})
          break
      }
    },
    coordinationUpdated (state) {
      state.coordinationUpdated = !state.coordinationUpdated
    },
    setCoordinationSearchFiled (state, payload) {
      state.coordinationSearchFiled = payload
    }
  }
}