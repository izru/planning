import Vue from 'vue'
import axios from 'axios'
import io from 'socket.io-client'

export default {
  actions: {
    async login({dispatch, commit}, { email, password }) {
      try {
        await axios.post(`${this.state.url}/auth/local`, {
          identifier: email,
          password: password,
        })
        .then(response => {
          const token = response.data.jwt
          const user = response.data.user
          const userEmail = response.data.user.email
          Vue.cookie.set('token', token)
          Vue.cookie.set('checkToken', new Date())
          localStorage.setItem('user', JSON.stringify(user))
          localStorage.setItem('userEmail', userEmail)
          axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
          commit('setSocket', io.connect(process.env.VUE_APP_URL, {
            query: { token, user: JSON.stringify(user) },
            transports: ['polling']
          }))
          this.state.socket.emit('login')
        })
      } catch (e) {
        throw e
      }
    },
    async logout({dispatch, commit}) {
      try {
        Vue.cookie.delete('token')
        Vue.cookie.delete('checkToken')
      } catch (e) {
        throw e
      }
    },
    async forgotPassword({dispatch, commit}, { email }){
      try {
        await axios.post(`${this.state.url}/auth/forgot-password`, {
          email: email
        })
      } catch (e) {
        throw e
      }
    },
    async resetPassword({dispatch, commit}, { code, password, confirmPassword }){
      try {
        await axios.post(`${this.state.url}/auth/reset-password`, {
          code: code,
          password: password,
          passwordConfirmation: confirmPassword,
        })
      } catch (e) {
        throw e
      }
    }
  }
}