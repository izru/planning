import moment from 'moment'

export default {
  state: {
    accreditationDate: moment(new Date()).set({hour:0,minute:0,second:0,millisecond:0}).format(),
    themeId: null
  },
  mutations: {
    setAccreditationDate (state, payload) {
      state.accreditationDate = moment(payload).set({hour:0,minute:0,second:0,millisecond:0}).format()
    },
    setAccreditationThemeId (state, payload) {
      state.themeId = payload
    }
  }
}